import * as T from '../Types';


const initialState ={
    name : '',
    password : '',
    email : '',
    loginState : false,
    registerState : false,
    bookList : [],
    popBookList : [],
    bookDetail : {},
    loginStatus : false,
    registerStatus : false,
    tokenValue : '',

};


const Reducer = (state = initialState, action) => {
    const {
        type,
        nameLoad,
        passLoad,
        emailLoad,
        regStateLoad,
        listDataLoad,
        listDetailLoad,
        loginCondition,
        registerCondition,
        tokenValLoad,
        popBookLoad,

    } = action;
    switch (type){
        case T.FETCH_BOOK_DETAIL :
            return {...state, bookDetail : listDetailLoad};
        case T.FETCH_BOOK_LIST : 
            return {...state, bookList : listDataLoad };
        case T.POST_LOGIN_BUTTON :
            return {...state, email : emailLoad, password : passLoad};
        case T.POST_REGISTER_BUTTON : 
            return {...state, name : nameLoad, password : passLoad, email : emailLoad};
        case T.REGISTER_SUCCES :
            return {...state, registerState : regStateLoad};
        case T.LOGIN_STATUS :
            return {...state, loginStatus : loginCondition};
        case T.REGISTER_STATUS :
            return {...state, registerStatus : registerCondition};
        case T.SET_TOKEN_ACCES : 
            return {...state, tokenValue : tokenValLoad};
        case T.POPULAR_BOOKS : 
            return {...state, popBookList : popBookLoad};
        default : 
            return state;
    }
};

export default Reducer;