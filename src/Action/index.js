import axios from 'axios';
import * as T from '../Types';


export const loginFunc = (email, password) => ({
    type: T.POST_LOGIN_BUTTON,
    emailLoad : email,
    passLoad : password,

}); 

export const registerFunc = (fullName, password, email) => ({
    type: T.POST_REGISTER_BUTTON,
    nameLoad : fullName,
    passLoad : password,
    emailLoad : email,
});

export const registerSucces = (registerState) => ({
    type: T.REGISTER_SUCCES,
    regStateLoad : registerState,
});

export const fetchBookList = (listData) => ({
    type : T.FETCH_BOOK_LIST,
    listDataLoad : listData,
});

export const fetchBookDetail = (listDetailData) =>({
    type : T.FETCH_BOOK_DETAIL,
    listDetailLoad : listDetailData,
});

export const loginStatus = (loginState) => ({
    type : T.LOGIN_STATUS,
    loginCondition : loginState
});

export const registerStatus = (registerState) => ({
    type : T.REGISTER_STATUS,
    registerCondition : registerState
});

export const setToken = (tokenVal) => ({
    type : T.SET_TOKEN_ACCES,
    tokenValLoad : tokenVal
});

export const popularBook  = (listPopBook) => ({
    type : T.POPULAR_BOOKS,
    popBookLoad : listPopBook 
});



// CONSUME API

export const booksList = (tokenValue) =>{
    return async(dispatch) => {
        const books = await axios.get('http://code.aldipee.com/api/v1/books', {
            headers : {
                'Authorization' : `Bearer ${tokenValue}`
            } 
        })
        .catch((err) => {
            console.log("ERR", err);
        });
        if(books.data.results.length > 0){
            dispatch(fetchBookList(books.data.results));
        }
    };
};

export const popularList = (tokenValue) => {
    return async(dispatch) => {
        const books = await axios.get('http://code.aldipee.com/api/v1/books', {
            headers : {
                'Authorization' : `Bearer ${tokenValue}`
            } 
        })
        .catch((err) => {
            console.log("ERR", err);
        });
        if(books.data.results.length > 0){
            dispatch(popularBook(books.data.results.sort((min,max) => {
                return max.average_rating - min.average_rating;
            })));
        }
    }
}

export const postLogin = (email, password) =>{
    return async(dispatch) =>{
        await axios.post(
            'http://code.aldipee.com/api/v1/auth/login',
            {
                email : email,
                password : password, 
            }
            ).then((response) =>{
                let searchToken = response.data.tokens.access.token;
                if(searchToken != null){
                    loginStatus(true);
                    dispatch(setToken(searchToken));
                    console.log('LOGIN SUCCES');
                };
            }).catch(err =>{
                console.log("ERR", err);
                console.log('LOGIN FAILED');
            });
    }
};

export const postRegister = (email, password, name) =>{
    return async() =>{
        await axios.post(
            'http://code.aldipee.com/api/v1/auth/register',
            {
                email : email,
                password : password,
                name : name,
            }
        ). then((response) => {
            if(response.data.success == true){
                registerStatus(true);
                console.log('REGISTER SUCCESS');
            }
        });
    }
}


export const getBookDetail = (bookID, tokenValue) => {
    return async(dispatch)=>{
        const bookDetail = await axios.get(`http://code.aldipee.com/api/v1/books/${bookID}`,
            {
                headers : {
                    'Authorization' : `Bearer ${tokenValue}`
                } 
            }).catch(err => {
                console.log("ERR", err);
            });
            dispatch(fetchBookDetail(bookDetail.data));
    }
};