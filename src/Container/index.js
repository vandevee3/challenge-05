import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomePage from './home';

const Stack = createNativeStackNavigator();

export default function MainScreen(props){
    return(
        <NavigationContainer>
          <Stack.Navigator initialRouteName ='Home'>
            <Stack.Screen name='Home' component={HomePage} options={{headerShown : false}}/>
          </Stack.Navigator>
        </NavigationContainer>
    );
}