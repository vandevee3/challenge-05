import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';


import { useDispatch, useSelector } from 'react-redux';
import { postRegister } from '../Action';

export default function RegisterPage({navigation}){
    const dispatch = useDispatch();
    const [email, onChangeEmail] = useState('');
    const [password, onChangePass] = useState('');
    const [name, onChaneName] = useState('');

    let nameRegist = '';
    let emailRegist =  '';
    let passwordRegist = '';

    function register(nameInput, emailInput, passwordInput){
        nameRegist = nameInput;
        emailRegist = emailInput;
        passwordRegist = passwordInput;
        dispatch(postRegister(emailRegist, passwordRegist, nameRegist));
        setTimeout(() => {
            navigation.navigate('Register Success');
        }, 3000);
    };

    return(
        <View style={styles.mainContainer} testID="registerView">
            <View style={styles.registerContainer}>
                <View style={styles.registerRequire}>
                    <TextInput style={styles.inputText} placeholder="Full Name" defaultValue={name} onChangeText={(name) => onChaneName(name)} testID="nameInput"/>
                    <TextInput style={styles.inputText} placeholder="Email" defaultValue={email} onChangeText={(email) => onChangeEmail(email)} testID="emailInput"/>
                    <TextInput style={styles.inputText} placeholder="Password" value={password} onChangeText={(password) => onChangePass(password)} secureTextEntry={false} testID="passwordInput"/>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.registerButton} onPress={()=>register(name,email,password)} testID="registerButton">
                        <Text style={styles.registerButtonText}>Register</Text>
                    </TouchableOpacity>
                </View>
           </View>
        </View>
    );
}

const styles = StyleSheet.create({
    mainContainer : {
        width : '100%',
        height : '100%',
        backgroundColor : '#e7e7e7'
    },
    
    registerContainer : {
        paddingLeft : '5%',
        paddingRight : '5%',
        marginTop : '50%'
    },  
    
    inputText : {
        height: 40,
        margin: 12,
        borderWidth: 2,
        padding: 10,   
    },

    buttonContainer : {
        justifyContent: 'center',
        alignItems : 'center',
    },
    registerButton : {
        backgroundColor : 'white',
        width : '19%',
        height : '35%',
        justifyContent: 'center',
        alignItems : 'center',
    },
    registerButtonText : {
        color : 'black'
    },
});