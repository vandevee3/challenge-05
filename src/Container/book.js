import React, {useEffect, useState, useCallback} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Share,
  RefreshControl
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { useDispatch, useSelector } from 'react-redux';

import Icon from 'react-native-vector-icons/Fontisto';
import Icon2 from 'react-native-vector-icons/Entypo';
import Star from 'react-native-vector-icons/AntDesign';

import { notifikasi } from './notification';


export default function BookPage({navigation}){

    const bookDetails = useSelector(state=>state.appData.bookDetail);
    console.log(bookDetails);

    const onShare = async () => {
        try {
          const result = await Share.share({
            message:
              'React Native | A framework for building native apps using React',
          });
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
      };

    function convertNumberToRupiah(input){
      let result = '';
      let rupiah = '';
      let tmpRev = input.toString().split('').reverse().join('');
      for(let i = 0; i < tmpRev.length; i++){
        if(i % 3 == 0){
          rupiah += tmpRev.substr(i,3)+'.';
        }
      }
      result = rupiah.split('',rupiah.length-1).reverse().join('');
      return result;
    }
    
    const likeButton = (bookTitle) =>{
      notifikasi.configure();
      notifikasi.buatChannel("1");
      notifikasi.kirimNotifikasi("1", "Like Notification", `Kamu menyukai ${bookTitle}`);
    }


    const[refreshing, setRefreshing] = useState(false);
    const[done, setDone] = useState(false);
    const wait = (timeout) => {
      return new Promise(resolve => setTimeout(resolve, timeout));
    }    
  
    const onRefresh = useCallback(()=>{
        setRefreshing(true);
        console.log('REFRESHING');
        wait(2000).then(()=>setRefreshing(false));
      }, []);

    return(
        <View style={styles.mainContainer}>
            <View style={styles.topNav}>
                <TouchableOpacity   TouchableOpacity style={styles.backButton} onPress={()=> navigation.navigate('Home')}>
                    <Icon2 name="back" size={30} color={'black'}/>
                </TouchableOpacity>
                <View style={styles.shareLikeContainer}>
                    <TouchableOpacity style={styles.likeButton} onPress={()=>likeButton(bookDetails.title)}>
                        <Icon2 name="heart" size={35} color={'black'} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.shareButton} onPress={onShare}>
                          <Icon name="share-a" size={25} color={'black'}/>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.bookInfoContainer}>
                <Image style={styles.imageStyle} source={{uri: bookDetails.cover_image}}/>
                <View style={styles.bookInfo}>
                    <Text style={{color : 'black', fontSize : 15}}>{bookDetails.title}</Text>
                    <Text style={{color : 'black', fontSize : 15}}>{bookDetails.author}</Text>
                    <Text style={{color : 'black', fontSize : 15}}>{bookDetails.publisher}</Text>
                </View>
            </View>
            <View style={styles.bookShortInfo}>
                <View style={styles.ratingBook}>
                    <Text style={{color : 'black'}}>Rating</Text>
                    <Text style={{color : 'black'}}><Star name="star" style={{color : 'yellow'}} size={18}/>{'\t'}{bookDetails.average_rating}</Text>
                </View>
                <View style={styles.totalSaleBook}>
                    <Text style={{color : 'black'}}>Total Sale</Text>
                    <Text style={{color : 'black'}}>{bookDetails.total_sale}</Text>
                </View>
                <View style={styles.buyContainer}>
                    <TouchableOpacity style={styles.buyButton}>
                        <Text style={{color : 'white'}}>{`Buy Rp. ${convertNumberToRupiah(bookDetails.price)}`}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.synopsisContainer}>
                <Text style={{color : 'black', fontSize : 20}}>Overview</Text>
                <View style={{paddingBottom : 20, marginRight : 10, height : '70%'}}>
                  <ScrollView RefreshControl={
                                    <RefreshControl 
                                        refreshing={refreshing}
                                        onRefresh={onRefresh}
                                    />
                                }>
                    <Text style={styles.synopsisText}>{bookDetails.synopsis}</Text>
                  </ScrollView>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    mainContainer :{
        width : '100%',
        height : '100%'
    },
    topNav : {
        paddingTop : 10,
        paddingLeft : 8,
        paddingRight : 8,
        flexDirection : 'row',
        justifyContent : 'space-between',
      },
      shareLikeContainer : {
        flexDirection : 'row'
      },
      shareButton : {
        marginLeft : 10,
      }, 
      imageStyle : {
        width : 100,
        height : 120
      },
      bookInfoContainer : {
        marginLeft :'5%',
        flexDirection : 'row',
        marginTop : '10%',
        marginBottom : '5%'
      },
      bookInfo : {
        paddingLeft : '5%',
        flexWrap : 'wrap'
      },
      bookShortInfo : {
        paddingLeft : '5%',
        flexDirection : 'row',
        justifyContent : 'space-around',
        paddingRight : '5%',
        marginTop : '5%'
      },
      synopsisContainer : {
        marginLeft  : '5%',
        marginRight : '5%',
        marginTop : '10%'
      },
      synopsisText : {
        textAlign : 'justify',
        color : 'black',
        fontSize : 15
      },
      ratingBook : {
        justifyContent : 'center',
        alignItems : 'center'
      },
      totalSaleBook : {
        justifyContent : 'center',
        alignItems : 'center'
      },
      buyContainer : {
        justifyContent : 'center',
        alignItems : 'center'
      },
      buyButton : {
        width : 120,
        height : 40,
        backgroundColor : 'blue',
        justifyContent : 'center',
        alignItems : 'center'
      }

});