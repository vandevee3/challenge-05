import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import SplashScreen from 'react-native-splash-screen';

import { useDispatch, useSelector } from 'react-redux';
import { postLogin } from '../Action';

import NetInfo from '@react-native-community/netinfo';
// import Video from 'react-native-video';

export default function LoginPage ({navigation}){

    const dispatch = useDispatch();
    const [email, onChangeEmail] = useState('');
    const [password, onChangePass] = useState('');
    const[connection, setConnection] = useState(false);

    let idLoggin ='';
    let passwordLogin = '';

    useEffect(() =>{
        SplashScreen.hide();
        NetInfo.fetch().then(state=>{
            if(state.isConnected == true){
                setConnection(true);
                console.log('Connection succes!');
            }else{
                setConnection(false);
                console.log('Connection failed!');
            }
        })
    },[]);

    function login(emailInput, passwordInput){
        idLoggin = emailInput;
        passwordLogin = passwordInput;
        dispatch(postLogin(idLoggin, passwordLogin));
        setTimeout(() => {
            navigation.navigate('Home');
        }, 1000);
    }

    return(
       <View style={styles.mainContainer}  testID="loginView">
           <View style={styles.loginContainer}>
                <View style={styles.videoInput}>
                 {/* <Video 
                          source={{uri : 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4'}}
                          style={{
                              width : '100%',
                              height : 100,
                              alignItems : 'center',
                              justifyContent : 'center'
                          }}
                  /> */}
                </View>
                <View style={styles.loginRequire}>
                     <TextInput style={styles.inputText} placeholder="Email" defaultValue={email} onChangeText={(email) => onChangeEmail(email)} testID="emailInput"/>
                     <TextInput style={styles.inputText} placeholder="Password" value={password} onChangeText={(password) => onChangePass(password)} secureTextEntry={true} testID="passwordInput"/>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.loginButton} onPress={()=>login(email,password)} testID="loginButton">
                        <Text style={styles.loginButtonText}>Login</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.dontHaveAccount}>
                    <Text>Dont Have Account?</Text>
                    <TouchableOpacity style={styles.registerButton} onPress={()=>navigation.navigate('Register')} testID="registerButton">
                        <Text style={styles.registerButtonText}>Register</Text>
                    </TouchableOpacity>
                </View>
           </View>
       </View>
    );
}

const styles = StyleSheet.create({
    mainContainer : {
        height : '100%',
        width : '100%',
        backgroundColor : '#e7e7e7'
    },

    loginContainer : {
        paddingLeft : '5%',
        paddingRight : '5%',
        marginTop : '10%'
    },  
    
    inputText : {
        height: 40,
        margin: 12,
        borderWidth: 2,
        padding: 10,   
    },

    buttonContainer : {
        justifyContent: 'center',
        alignItems : 'center',
    },
    loginButton : {
        backgroundColor : 'white',
        width : '19%',
        height : '35%',
        justifyContent: 'center',
        alignItems : 'center',
    },
    loginButtonText : {
        color : 'black'
    },
    dontHaveAccount : {
        justifyContent : 'center',
        alignItems : 'center'
    },
    videoInput : {
        justifyContent : 'center',
        alignItems : 'center',
        paddingLeft : '20%'
    },
    loginRequire : {
        marginTop : '10%'
    }

});
