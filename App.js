import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// import LoginPage from './src/Container/login';
// import RegisterPage from './src/Container/register';

import HomePage from './src/Container/home';
import LoginPage from './src/Container/login';
import RegisterPage from './src/Container/register';
import RegisterSuccess from './src/Container/registerSucces';
import BookPage from './src/Container/book';


import {store} from './src/Store'
import { Provider } from 'react-redux';



const Stack = createNativeStackNavigator();

 export default function App (props){
    return(
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName = 'Login'>
            <Stack.Screen name='Login' component={LoginPage} options={{headerShown :false}}/>
            <Stack.Screen name='Home' component={HomePage} options={{headerShown :false}}/>
            <Stack.Screen name='Register' component={RegisterPage} options={{headerShown : false}}/>
            <Stack.Screen name='Register Success' component={RegisterSuccess} options={{headerShown : false}}/>
            <Stack.Screen name='Book' component={BookPage} options={{headerShown : false}}/>
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
 }


