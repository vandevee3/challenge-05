describe('Example', () => {
  beforeAll(async () => {
    await device.launchApp({newInstance : true});
  });

  beforeEach(async () => {
    // await device.reloadReactNative();
  });

  it('should have login screen', async () => {
    await expect(element(by.id('loginView'))).toBeVisible();
  });

  it('click register button', async () => {
    await element(by.id('registerButton')).tap();
  });

  it('should have register screen', async () => {
    await expect(element(by.id('registerView'))).toBeVisible();
  });

  it('input account register identity and click register', async () => {
    await element(by.id('nameInput')).typeText('vegacoba7');
    await element(by.id('emailInput')).typeText('vegacoba07@gmail.com');
    await element(by.id('passwordInput')).typeText('vega12345');
    await element(by.id('registerButton')).tap;
  });


  it('should have register succes screen', async () => {
    await expect(element(by.id('registerSuccessView'))).toBeVisible();
  });

});
