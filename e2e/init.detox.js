beforeAll(
    async () => {
      await detox.init(config);
      await detox.device.launchApp({ newInstance: true });
    },
    // 300sec or 5min
    30 * 1000,
  );