import 'react-native';
import React from 'react';
import renderer, { create } from 'react-test-renderer';
import data from '../dummy/datadummy';
import * as Action from '../src/Action/index';
import LoginPage from '../src/Container/login';
import {store} from '../src/Store'


describe('Login', () => {
    test('Login Page Snapshot', () =>{
        const snap = renderer.create(<LoginPage/>).toJSON();
        expect(snap).toMatchSnapshot();
    });

    
    // let LoginData = renderer.create(<LoginPage />).getInstance();
    // LoginData.lo

    it('Dispatch LOGIN', async () =>{
        await store.dispatch(Action.postLogin('vegacoba4@gmail.com', 'vega12345')).then((response) => {
                expect(response).toEqual(data);
            })
    });
});



