import 'react-native';
import React from 'react';
import MainScreen from '../src/Container/index';
import renderer from 'react-test-renderer';

test('Home Snapshot', () =>{
    const snap = renderer.create(<MainScreen/>).toJSON();
    expect(snap).toMatchSnapshot();
});