import 'react-native';
import React from 'react';
import RegisterPage from '../src/Container/register';
import renderer from 'react-test-renderer';
import * as Action from '../src/Action/index';
import {store} from '../src/Store';


describe('Resgiter', () => {
    
    test('Register Snapshot', () =>{
        const snap = renderer.create(<RegisterPage/>).toJSON();
        expect(snap).toMatchSnapshot();
    });

    it('Dispatch REGISTER', async () =>{
        await store.dispatch(Action.postRegister('vegacoba012@gmail.com', 'vega12345', 'Vega Coba'));
    });
})